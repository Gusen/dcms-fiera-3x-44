<?php

#	панель для форм .вывод смайлов и бб кодов
class panel_form
{
    #	верх форм
    static function head()
    {

?>
<script src="/sys/js/bbscript/bb.script.js" type=
    "text/javascript">
</script>
    <div class='panel_form'>
      <a href="javascript:tag('%20.rad.%20',%20'%20')"><img src=
      "/style/smiles/rad_subdomain.test1.ru" alt="Смайл" title=
      "Смайл" /></a> <a href=
      "javascript:tag('%20.%D0%B7%D1%83%D0%B1%D1%8B.%20',%20'%20')">
      <img src="/style/smiles/zuby_subdomain.test1.ru" alt="Смайл"
      title="Смайл " /></a> <a href=
      "javascript:tag('%20.yjas.%20',%20'%20')"><img src=
      "/style/smiles/uzhas_subdomain.test1.ru" alt="Смайл" title=
      "Смайл " /></a> <a href=
      "javascript:tag('%20.pesh.%20',%20'%20')"><img src=
      "/style/smiles/pechal_subdomain.test1.ru" alt="Смайл" title=
      "Смайл " /></a> <a href=
      "javascript:tag('%20.yaz.%20',%20'%20')"><img src=
      "/style/smiles/jazyk_subdomain.test1.ru" alt="Смайл " title=
      "Смайл " /></a> <a href=
      "javascript:tag('%20.%D0%B1%D0%B5%D0%B7%D0%97%D0%B2%D1%83%D0%BA%D0%B0.%20',%20'%20')">
      <img src="/style/smiles/bez_zvuka_subdomain.test1.ru" alt=
      "Смайл " title="Смайл " /></a> :: <a href=
      '/pages/smiles/'><?=lang('Все смайлы')?></a>
    </div>
<?

    }
    #	низ форм
    static function foot()
    {

?>
<script src="/sys/js/bbscript/bb.script.js" type=
    "text/javascript">
</script>
    <div class='panel_form'>
      <a href=
      "javascript:tag('[url=http://%D0%B0%D0%B4%D1%80%D0%B5%D1%81]%D0%BD%D0%B0%D0%B7%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5',%20'[/url]')">
      <img src="/sys/js/bbscript/img/l.png" alt="url" title=
      "Ссылка" /></a> <a href=
      "javascript:tag('[b]',%20'[/b]')"><img src=
      "/sys/js/bbscript/img/b.png" alt="b" title="Жирный" /></a>
      <a href="javascript:tag('[i]',%20'[/i]')"><img src=
      "/sys/js/bbscript/img/i.png" alt="i" title="Наклонный" /></a>
      <a href="javascript:tag('[u]',%20'[/u]')"><img src=
      "/sys/js/bbscript/img/u.png" alt="u" title=
      "Подчёркнутый" /></a> <a href=
      "javascript:tag('[color=#a00000]',%20'[/color]')"><img src=
      "/sys/js/bbscript/img/re.png" alt="red" title=
      "Красный" /></a> <a href=
      "javascript:tag('[color=#008000]',%20'[/color]')"><img src=
      "/sys/js/bbscript/img/gr.png" alt="green" title=
      "Зелёный" /></a> <a href=
      "javascript:tag('[color=#0000ff]',%20'[/color]')"><img src=
      "/sys/js/bbscript/img/bl.png" alt="blue" title="Синий" /></a>
      :: <a href='/pages/bbcodes/'><?=lang('Все бб коды')?></a>
    </div>
<?

    }
}