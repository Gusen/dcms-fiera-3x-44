<?php

$set['title'] = 'Поиск по форуму';
include_once '../sys/inc/thead.php';
title().aut();

if (isset($_POST['search']) || isset($_SESSION['search'])) {
    if (!isset($_POST['search']) && isset($_SESSION['search'])) {
        $search = $_SESSION['search'];
    } else {
        $search = mysql_real_escape_string(trim($_POST['text']));
        $_SESSION['search'] = mb_strtolower($search);
    }
    if (mb_strlen($search) < 3) {
        ?>
        <div class = 'err'>Слишком короткий текст.</div>
        <?
    } else {
        $k_post = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_themes` WHERE `name` LIKE "%'.$_SESSION['search'].'%" OR `description` LIKE "%'.$_SESSION['search'].'%"'), 0);
        $k_page = k_page($k_post, $set['p_str']);
        $page = page($k_page);
        $start = $set['p_str']*$page-$set['p_str'];
        if ($k_post == 0) {
            ?>
            <div class = 'err'>По Вашему запросу ничего не найдено.</div>
            <?
        } else {
            ?>
            <div class = 'p_m' style = 'text-align: right'>Вы ввели: <b><?= output_text($_SESSION['search']) ?></b></div>
            <table class = 'post'>
                <?
                $themes = mysql_query('SELECT * FROM `forum_themes` WHERE `name` LIKE "%'.$_SESSION['search'].'%" OR `description` LIKE "%'.$_SESSION['search'].'%" ORDER BY `time_post` ASC LIMIT '.$start.', '.$set['p_str']);
                while ($theme = mysql_fetch_object($themes)) {
                    $theme->name = str_replace($_SESSION['search'], '[color=yellow]'.$_SESSION['search'].'[/color]', mb_strtolower($theme->name));
                    $theme->description = str_replace($_SESSION['search'], '[color=yellow]'.$_SESSION['search'].'[/color]', mb_strtolower($theme->description));
                    $razdel = mysql_fetch_object(mysql_query('SELECT `id`, `id_forum` FROM `forum_razdels` WHERE `id` = '.$theme->id_razdel));
                    $forum = mysql_fetch_object(mysql_query('SELECT `id`, `access` FROM `forum` WHERE `id` = '.$razdel->id_forum));
                    if ($forum->access == 0 || (isset($user) && $forum->access == 1 && $user['group_access'] > 7) || (isset($user) && $forum->access == 2 && $user['group_access'] > 1)) {
                        $count_posts = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_posts` WHERE `id_theme` = '.$theme->id), 0);
                        if ($theme->reason_close != NULL) {
                            $type = '_close';
                        } elseif ($theme->type == 1) {
                            $type = '_up';
                        } else {
                            $type = NULL;
                        }
                        ?>
                        <tr>
                            <td class = 'icon14'>
                                <img src = '<?= FORUM ?>/icons/theme<?= $type ?>.png' alt = '' <?= ICONS ?> />
                            </td>
                            <td class = 'p_t'>
                                <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'><?= output_text($theme->name) ?></a> (<?= $count_posts ?>) - <?= vremja($theme->time) ?>
                            </td>
                        </tr>
                        <tr>
                            <td class = 'p_m' colspan = '2'>
                                <?= output_text($theme->description) ?>
                            </td>
                        </tr>
                        <?
                    } else {
                        continue;
                    }
                }
                ?>
            </table>
            <?
            if ($k_page > 1) {
                str(FORUM.'/search/', $k_page, $page);
            }
        }
    }
}
?>
<form action = '<?= FORUM ?>/search.html' method = 'post' class="p_m">
    <b>Введите текст (мин. 3 символа):</b><br />
    <input type = 'text' name = 'text' value = '<?= (isset($_SESSION['search'])) ? output_text($_SESSION['search']) : NULL ?>' /><br />
    <input type = 'submit' name = 'search' value = 'Поиск' />
</form>
<div class = 'p_m' style = 'text-align: right'><a href = '<?= FORUM ?>'>Вернуться в форум</a></div>